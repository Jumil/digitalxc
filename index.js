
var readline = require('linebyline');

function questionAndAnswer(read) {
    let readLines = []; // for storing already asked question
    read.on('line', function (line, lineCount, byteCount) {

        if (line === "Hey. How are you?") {
            console.log("Hello, I am doing great.")
            readLines.push(line)
        } else if (line === "Clean my room.") {
            if (readLines.includes(line)) {
                console.log("The room was just cleaned 0 minute(s) ago. I hope it's not dirty")
            } else {
                var d = new Date(); // for now
                console.log("Room is cleaned. It looks tidy now. Job completed at " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds())
            }
            readLines.push(line)
        } else if (line === "Fetch the newspaper.") {
            if (readLines.includes(line)) {
                console.log("I think you don't get another newspaper the same day")
            } else {
                console.log("Here is your newspaper.")
            }
            readLines.push(line)
        } else if (line === "Read my shopping list.") {
            if (readLines.includes(line)) {
                console.log("Here is your shopping list. Bread, Eggs.")
            } else {
                console.log("You have no items in your shopping list")
            }
            readLines.push(line)
        } else if (line === "Add Bread to my shopping list.") {
            if (readLines.includes(line)) {
                console.log("You already have Bread in your shopping list")
            } else {
                console.log("Bread added to your shopping list")
            }
            readLines.push(line)
        } else if (line === "Add Eggs to my shopping list.") {
            if (readLines.includes(line)) {
                console.log("You already have Eggs in your shopping list")
            } else {
                console.log("Eggs added to your shopping list")
            }
            readLines.push(line)
        } else if (line === "How's the weather outside?") {
            console.log("It's pleasant outside. You should take a walk.")
            readLines.push(line)
        } else {
            console.log("Hmm.. I don't know that")
        }
    }).on('error', function (e) {
        console.log(e)
    });
}

let read = readline('./questions.txt'); //reading lines from the questions.txt file

questionAndAnswer(read);
